package hellocucumber;

public class SayHello {

    public static String sayHello() {
        return "Hello cucumber!";
    }

    public static void main(String[] args) {
        System.out.println(sayHello());
    }

}
