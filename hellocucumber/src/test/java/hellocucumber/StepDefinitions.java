package hellocucumber;

import io.cucumber.java.en.*;
import org.junit.jupiter.api.Assertions.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StepDefinitions {
    private String helloMessage;

    @Given("I have a hello message")
    public void iHaveHelloMessage() {

        helloMessage = SayHello.sayHello();
    }

    @When("I say hello")
    public void iSayHello() {

    }

    @Then("I should see the hello message")
    public void iShouldSeeHelloMessage() {
        assertEquals("Hello cucumber!", helloMessage);
    }

}
